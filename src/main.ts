/* eslint-disable @typescript-eslint/no-var-requires */
import { NestFactory } from '@nestjs/core'
import { ValidationPipe } from '@nestjs/common'
import { AppModule } from './app'
const cookieSession = require('cookie-session')

const PORT = 3000
async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  app.use(
    cookieSession({
      keys: ['abcdef']
    })
  )
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true
    })
  )
  await app.listen(PORT)
  console.log(`App running at port ${PORT}`)
}
bootstrap()
