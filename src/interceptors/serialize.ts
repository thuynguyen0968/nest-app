import {
  CallHandler,
  ExecutionContext,
  NestInterceptor,
  UseInterceptors
} from '@nestjs/common'
import { plainToInstance } from 'class-transformer'
import { Observable, map } from 'rxjs'

interface ClassConstructor {
  new (...args: any[]): object
}
export class SerializerInterCeptor implements NestInterceptor {
  constructor(private dtoIntances: ClassConstructor) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      map((data: any) => {
        return plainToInstance(this.dtoIntances, data, {
          excludeExtraneousValues: true
        })
      })
    )
  }
}

export function Serializer(dto: ClassConstructor) {
  return UseInterceptors(new SerializerInterCeptor(dto))
}
