import { IsEmail, IsString } from 'class-validator'
import { TimeStamp } from 'src/utils/common'
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class User extends TimeStamp {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  @IsString()
  @IsEmail()
  email: string

  @Column()
  @IsString()
  password: string

  // @AfterInsert()
  // logInsert() {
  //   console.log(`Insert User with id ${this.id}`)
  // }

  // @AfterRemove()
  // logRemove() {
  //   console.log(`Remove User with id ${this.id}`)
  // }

  // @AfterUpdate()
  // logUpdate() {
  //   console.log(`User ${this.id} was updated`)
  // }
}
