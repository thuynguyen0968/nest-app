import { Injectable, NotFoundException } from '@nestjs/common'
import { Repository } from 'typeorm'
import { User } from './auth.entity'
import { InjectRepository } from '@nestjs/typeorm'
import { messageResponse } from 'src/constants/statics'

@Injectable()
export class AuthService {
  constructor(@InjectRepository(User) private userRepo: Repository<User>) {}

  async create(body: User) {
    // 2 query
    const user = this.userRepo.create(body)
    return await this.userRepo.save(user)
  }

  async findOne(id: number) {
    if (!id) return null
    const user = await this.userRepo.findOneBy({ id })
    if (!user) throw new NotFoundException(messageResponse.notFoundUser)
    return user
  }

  async findAll(email?: string) {
    if (email) {
      return this.userRepo.find({ where: { email } })
    }
    return this.userRepo.find({})
  }

  async update(id: number, dataUpdate: Partial<User>) {
    // 2 query
    const user = await this.findOne(id)
    if (!user) throw new NotFoundException(messageResponse.notFoundUser)
    Object.assign(user, dataUpdate)
    return await this.userRepo.save(user)
  }

  async remove(id: number) {
    // this way use 2 query to db -> run hook
    const user = await this.findOne(id)
    if (!user) throw new NotFoundException(messageResponse.notFoundUser)
    return this.userRepo.remove(user)
  }
}
