import { Module } from '@nestjs/common'
import { AuthController } from './auth.controller'
import { AuthService } from './auth.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { User } from './auth.entity'
import { UserServices } from './user.service'
import { AuthInterceptor } from './auth.interceptor'
import { APP_INTERCEPTOR } from '@nestjs/core'

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  controllers: [AuthController],
  providers: [
    AuthService,
    UserServices,
    { provide: APP_INTERCEPTOR, useClass: AuthInterceptor }
  ]
})
export class AuthModule {}
