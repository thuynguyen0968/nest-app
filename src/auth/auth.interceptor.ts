import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor
} from '@nestjs/common'
import { AuthService } from './auth.service'

@Injectable()
export class AuthInterceptor implements NestInterceptor {
  constructor(private authServices: AuthService) {}

  async intercept(context: ExecutionContext, next: CallHandler) {
    const request = context.switchToHttp().getRequest()
    const { userId } = request.session || {}
    // if login -> get infomation
    if (userId) {
      const user = await this.authServices.findOne(userId)
      request.currentUser = user
    }

    // not found id -> not login -> go away
    return next.handle()
  }
}
