import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import { AuthService } from './auth.service'
import { User } from './auth.entity'
import { messageResponse } from 'src/constants/statics'
import { randomBytes, scrypt as _scrypt } from 'crypto'
import { promisify } from 'util'

const scrypt = promisify(_scrypt)

@Injectable()
export class UserServices {
  constructor(private readonly authServices: AuthService) {}

  async registerUser(body: User) {
    const users = await this.authServices.findAll(body.email)
    if (users.length) {
      throw new BadRequestException(messageResponse.emailHasExist)
    }
    const SALT = randomBytes(8).toString('hex')
    const hash = (await scrypt(body.password, SALT, 20)) as Buffer
    const hashPassword = `${SALT}:${hash.toString('hex')}`

    const user = await this.authServices.create({
      ...body,
      password: hashPassword
    })
    return user
  }

  async loginUser(body: User) {
    const [user] = await this.authServices.findAll(body.email)
    if (!user) {
      throw new NotFoundException(messageResponse.notCorrectLogin)
    }
    const [salt, storeHash] = user.password.split(':')
    const hash = (await scrypt(body.password, salt, 20)) as Buffer

    if (storeHash !== hash.toString('hex')) {
      throw new BadRequestException(messageResponse.notCorrectLogin)
    }
    return user
  }
}
