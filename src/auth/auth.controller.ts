import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Session
} from '@nestjs/common'
import { AuthService } from './auth.service'
import { User } from './auth.entity'
import { AuthDto } from './auth.dto'
import { Serializer } from 'src/interceptors/serialize'
import { UserServices } from './user.service'
import { CurrentUser } from './auth.decorator'

@Controller('auth')
@Serializer(AuthDto)
export class AuthController {
  constructor(
    private readonly authServices: AuthService,
    private readonly userServices: UserServices
  ) {}
  @Post('/register')
  async createUser(@Body() body: User, @Session() session: any) {
    const user = await this.userServices.registerUser(body)
    session.userId = user.id
    return user
  }

  @Post('/login')
  async loginUser(@Body() body: User, @Session() session: any) {
    const user = await this.userServices.loginUser(body)
    session.userId = user.id
    return user
  }

  @Get('/whoami')
  whoAmI(@CurrentUser() user: User) {
    return user
  }

  @Post('/logout')
  logOutUser(@Session() session: any) {
    return (session.userId = null)
  }
  @Get()
  findAllUser(@Query('email') email?: string) {
    return this.authServices.findAll(email)
  }

  @Get('/:id')
  findUserById(@Param('id') id: string) {
    return this.authServices.findOne(+id)
  }

  @Patch('/:id')
  updateUser(@Param('id') id: string, @Body() body: Partial<User>) {
    return this.authServices.update(+id, body)
  }

  @Delete('/:id')
  removeUser(@Param('id') id: string) {
    return this.authServices.remove(+id)
  }
}
