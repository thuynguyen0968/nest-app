import { ExecutionContext, createParamDecorator } from '@nestjs/common'

/**
 * This decorator return current user
 */
export const CurrentUser = createParamDecorator(
  (data: never, context: ExecutionContext) => {
    const request = context.switchToHttp().getRequest()
    return request.currentUser
  }
)
