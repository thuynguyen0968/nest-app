import { ReportsModule } from './reports.module'
import { Reports } from './reports.entity'

export { ReportsModule, Reports }
