import { TimeStamp } from 'src/utils/common'
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class Reports extends TimeStamp {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  name: string

  @Column()
  price: number
}
