export const messageResponse = {
  emailHasExist: 'Email has adready use',
  notFoundUser: 'Not found user',
  registerSuccedd: 'Register user successful',
  notCorrectLogin: 'Email or password not correct'
} as const

export const httpStatus = {
  OK: 200,
  NOT_FOUND: 404,
  CREATED: 201,
  BAD_REQUEST: 400,
  INTERNAL_SERVER: 500,
  UNAUTHORIZATION: 403
} as const
